# naxsi-docker

**WARNING: This project is from a personnal project only. The configuration is desastrous, not optimized and certainly not production ready.**

**Use at your own risk just for experimenting with naxsi**

## What is it

This is a container running nginx with [naxsi](https://github.com/nbs-system/naxsi)
The container use debian and use its own compiled version of nginx

## How to use

You can follow the docker compose and refer to [naxsi documentation](https://github.com/nbs-system/naxsi/wiki)

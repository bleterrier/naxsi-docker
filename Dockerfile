FROM debian:11

LABEL naxsi_commit="29793dcb2d9bdfa02c9b8d6968063ce3daa0a77b"
LABEL os_version="Debian 11"

RUN apt update -y && apt upgrade -y
RUN apt install gcc make libpcre3-dev wget libssl-dev zlib1g-dev libxml2-dev libxslt-dev libgd-dev libgeoip-dev libperl-dev -y

ENV NAXSI_VER=1.3
ENV NGINX_VER=1.18.0


WORKDIR /usr/app/

RUN wget https://github.com/nbs-system/naxsi/archive/$NAXSI_VER.tar.gz -O naxsi_$NAXSI_VER.tar.gz
RUN wget https://github.com/nbs-system/naxsi/releases/download/$NAXSI_VER/naxsi-$NAXSI_VER.tar.gz.asc -O naxsi_$NAXSI_VER.tar.gz.asc


RUN wget https://nginx.org/download/nginx-$NGINX_VER.tar.gz

RUN tar vxf naxsi_$NAXSI_VER.tar.gz
RUN tar vxf nginx-$NGINX_VER.tar.gz

WORKDIR /usr/app/nginx-$NGINX_VER/

RUN ./configure --with-pcre --prefix=/usr --user=root --group=root --with-threads --with-file-aio \
 --with-http_ssl_module --with-http_v2_module --with-http_realip_module --with-http_addition_module \
 --with-http_xslt_module=dynamic \
 --with-http_image_filter_module \
 --with-http_geoip_module=dynamic \
 --with-http_sub_module \
 --with-http_dav_module \
 --with-http_flv_module \
 --with-http_mp4_module \
 --with-http_gunzip_module \
 --with-http_gzip_static_module \
 --with-http_auth_request_module \
 --with-http_random_index_module \
 --with-http_secure_link_module \
 --with-http_degradation_module \
 --with-http_slice_module \
 --with-http_stub_status_module \
 --without-http_charset_module \
 --with-http_perl_module \
 --with-mail=dynamic \
 --with-mail_ssl_module \
 --with-stream=dynamic \
 --with-stream_ssl_module \
 --with-stream_realip_module \
 --with-stream_geoip_module=dynamic \
 --with-stream_ssl_preread_module \
 --add-dynamic-module=../naxsi-$NAXSI_VER/naxsi_src/

RUN make
RUN make install
RUN make modules

#RUN cp ./objs/ngx_http_naxsi_module.so /etc/nginx/modules-enabled/
RUN cp ./objs/ngx_http_naxsi_module.so /usr/conf/

WORKDIR /usr/app/naxsi-$NAXSI_VER/naxsi_config/
RUN ls -alh

RUN cp naxsi_core.rules /usr/conf/

RUN ln -sf /dev/stdout /usr/logs/access.log
RUN ln -sf /dev/stderr /usr/logs/error.log

EXPOSE 80

STOPSIGNAL SIGQUIT

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
